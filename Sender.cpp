#include <utility>
#include <iostream>

//
// Created by joseph on 10/27/18.
//

#include "Sender.hpp"
#include "Packet.hpp"
#include <cmath>
#include <chrono>

Sender::Sender(std::string &file, uint32_t wsize, uint32_t bsize, std::string &ip, in_port_t port) {
    this->initReaderHandler(file.c_str());
    this->sws = wsize;
    this->lar = 0;
    this->lfs = 0;
    this->buffer_size = bsize;
    this->setSocket(port, ip);
}

void Sender::initReaderHandler(const char * file) {
    this->reader_handler.reader_stream = fopen(file, "rb");
    if (this->reader_handler.reader_stream == nullptr) handleError("Fail to open file");
    stat(file, &(this->reader_handler.st));
    this->reader_handler.total_packets = (uint32_t) ceil(double(this->reader_handler.st.st_size) / MAX_DATA_LENGTH);
    this->reader_handler.packets_moved = 0;
    this->reader_handler.packets_in_buffer = 0;
}

void Sender::setSocketTimeout() {
    struct timeval tv{};
    tv.tv_sec = 0;
    tv.tv_usec = SENDER_TIMEOUT;
    setsockopt(this->net_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof tv);
}

void Sender::setSocket(in_port_t port, std::string &ip) {
    this->setNetSocket();
    this->setSocketTimeout();
    this->setReceiverAddr(std::move(ip), port);
    this->createConnection();
}

void Sender::setReceiverAddr(std::string ip, in_port_t port) {
    // Empty the address of receiver_addr first!
    memset((char *) &(this->address), 0, sizeof(this->address));
    this->address.sin_family = AF_INET;
    this->address.sin_port = htons(port);
    this->address.sin_addr.s_addr = inet_addr(ip.c_str());
}

void Sender::createConnection() {
    this->connection = connect(this->net_socket, (struct sockaddr *) &address, sizeof(address));
    if (this->connection == -1) {
        handleError("ERROR Create Connection!");
    }
}

void Sender::readFileToBuffer() {
    while (this->reader_handler.packets_in_buffer < this->buffer_size &&
            this->reader_handler.packets_moved < this->reader_handler.total_packets) {
        size_t buf_len = fread(this->reader_handler.buffer, 1, MAX_DATA_LENGTH, this->reader_handler.reader_stream);
        Packet p = createPacket(this->reader_handler.packets_moved + 1, this->reader_handler.buffer, buf_len);
        PacketSender ps;
        ps.time = 0;
        ps.p = p;
        // Add to buffer packets
        this->packets[reader_handler.packets_moved % this->buffer_size] = ps;
        this->reader_handler.packets_moved += 1;
        this->reader_handler.packets_in_buffer += 1;
    }
}

void Sender::run() {
    packets = new PacketSender[this->buffer_size];
    memset(this->packets, 0, this->buffer_size * sizeof(PacketSender));
    char buffer_socket[PACKET_LENGTH + 1];
    socklen_t addr_len = sizeof(this->address);
    while (this->reader_handler.packets_moved < this->reader_handler.total_packets) {
        // Read file to buffer packets
        this->readFileToBuffer();
        while ((this->reader_handler.packets_in_buffer > this->sws ||
                this->reader_handler.packets_moved == this->reader_handler.total_packets) &&
                this->lar < this->reader_handler.total_packets) {
            // Send as long as you can!!!
            while (this->lfs - this->lar <= this->sws && this->lfs <= this->reader_handler.total_packets) {
                PacketSender* ps = &(this->packets[this->lfs - this->lar]);
                char *buffer = serializePacket(ps->p, ps->p.data_length + 10);
                if (send(this->net_socket, buffer, ps->p.data_length + 10, 0) >= 0) {
                    this->packets[this->lfs - this->lar].time = static_cast<uint64_t>(std::time(nullptr));
                    this->lfs += 1;
                    std::cout << "Transmitting Packet " << this->lfs << std::endl;
                }
                ps->time = static_cast<uint64_t>(std::time(nullptr));
            }
            ssize_t buf_len = recvfrom(this->net_socket, buffer_socket, sizeof(buffer_socket), 0,
                                       (struct sockaddr *) &this->address, &addr_len);
            if (buf_len >= 0) {
                ACK ack;
                if (unserializeACK(buffer_socket, (size_t) buf_len, ack)) {
                    if (ack.next_sequence_number <= this->lar) break;
                    if (ack.ACK == ACK_NUMBER) {
                        uint32_t next_sequence_number = ack.next_sequence_number;
                        for (int num_packet = this->lar; num_packet < next_sequence_number - 1; num_packet++) {
                            memset(&this->packets[num_packet % this->buffer_size], 0, sizeof(PacketSender));
                            this->reader_handler.packets_in_buffer -= 1;
                        }
                        this->lar = next_sequence_number - 1;
                        std::cout << "Get ACK " << ack.next_sequence_number << std::endl;
                        PacketSender* p = &(this->packets[this->lar % buffer_size]);
                        std::cout << "Seq number " << p->p.sequence_number << std::endl;
                    } else {
                        std::cout << "Get NAK " << ack.next_sequence_number << std::endl;
                        PacketSender* ps = &(this->packets[(ack.next_sequence_number-1)% this->buffer_size]);
                        char* buffer = serializePacket(ps->p, ps->p.data_length + 10);
                        while (send(this->net_socket, buffer, ps->p.data_length + 10, 0) < 0) {}
                        ps->time = static_cast<uint64_t>(std::time(nullptr));
                        std::cout << "Packet " << ps->p.sequence_number << " " << ps->time << std::endl;
                    }
                }
            }
            // Check for timeout!!
            long time = static_cast<long >(std::time(nullptr));
            for (int num_packet = this->lar; num_packet < this->lfs; num_packet++) {
                PacketSender* ps = &(this->packets[num_packet % this->buffer_size]);
                char* buffer = serializePacket(ps->p, ps->p.data_length + 10);
//                std::cout << "Checking packet timeout " <<  ps->p.sequence_number << " " << ps->time << std::endl;
                if (time - (long) ps->time > PACKET_TIMEOUT) {
                    std::cout << "Trying to retransmit data " << ps->p.sequence_number << std::endl;
                    while (send(this->net_socket, buffer, ps->p.data_length + 10, 0) < 0) {}
                    ps->time = (uint64_t) time;
                }
            }
        }
    }
    Packet p = createEndPacket(this->lar + 1);
    char* buffer = serializePacket(p, p.data_length + 10);
    while (send(this->net_socket, buffer, p.data_length + 10, 0) < 0) {}
    printf("Finished sending data\n");
    fclose(this->reader_handler.reader_stream);
}