//
// Created by joseph on 10/27/18.
//

#ifndef JARKOM_RECEIVER_H
#define JARKOM_RECEIVER_H

#include "UDPSocket.hpp"

typedef struct {
    FILE* output_stream;
    char* buffer_stream;
    uint32_t buffer_length;
} FileWriter;

class Receiver: public UDPSocket {
private:
    int binding;
    uint32_t rws, laf, lfr; // Receiving Windows Size, Largest Acceptable Frame, Last Frame Received
    struct sockaddr_in client_addr;
    bool end = false;
    Packet* packets;
    FileWriter writer_handler;
    // Methods
    void setServerAddress(in_port_t port);
    void setBinding();
    void setSocket(in_port_t);
    void setSocketTimeout();
    bool checkPacketWithinRWS(Packet& p);
    bool checkBeforeLFR(Packet& p);
    void handlePacket(char* buffer, size_t buf_len);
    void addToPackets(Packet& p);
    void addToFileBuffer(Packet& p);
    void sendNAK();
    void sendACK();
    bool checkSendACK();
public:
    Receiver(std::string file, uint32_t wsize, uint32_t bsize, in_port_t port);
    void run() override;
};

#endif //JARKOM_RECEIVER_H
