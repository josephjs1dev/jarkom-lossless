//
// Created by joseph on 10/27/18.
//

#ifndef _PACKET_H
#define _PACKET_H

#include <stdint.h>
#include <stddef.h>
#include "Utils.hpp"

#define SOH_NUMBER 0x01
#define ACK_NUMBER 0x06
#define NAK_NUMBER 0x21
#define EOT 0x04
#define PACKET_LENGTH 1034
#define MAX_DATA_LENGTH 1024
#define PACKET_OFFSET_SOH 0
#define PACKET_OFFSET_SEQUENCE 1
#define PACKET_OFFSET_DATA_LENGTH 5
#define PACKET_OFFSET_DATA 9
#define PACKET_OFFSET_CHECKSUM -1
#define ACK_LENGTH 6
#define ACK_OFFSET_ACK 0
#define ACK_OFFSET_SEQUENCE 1
#define ACK_OFFSET_CHECKSUM 5

typedef struct {
    unsigned char SOH; // 1
    uint32_t sequence_number; // 4
    size_t data_length; // 4
    char data[1024]; // 1024
    uint8_t checksum; // 1
} Packet;

typedef struct {
    unsigned char ACK;
    uint32_t next_sequence_number;
    uint8_t checksum;
} ACK;

typedef struct {
    uint64_t time;
    Packet p;
} PacketSender;

Packet createPacket(uint32_t sequence_number, const char data[], size_t length);
Packet createEndPacket(uint32_t sequence_number);
ACK createACK(bool negative, uint32_t sequence_number);

bool unserializePacket(char* buffer, size_t length, Packet &p);
bool unserializeACK(char* buffer, size_t length, ACK &ack);

uint8_t calculatePacketCheckSum(Packet &p);
uint8_t calculateACKCheckSum(ACK &ack);

char * serializePacket(Packet &p, size_t length);
char * serializeACK(ACK &ack);

bool verifyPacketCheckSum(Packet &p);
bool verifyACKCheckSum(ACK &ack);

#endif