//
// Created by joseph on 10/27/18.
//

#ifndef JARKOM_UDPSOCKET_H
#define JARKOM_UDPSOCKET_H

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <string.h>
#include <string>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include "Utils.hpp"
#include "Packet.hpp"

#define RECEIVE_TIMEOUT 10
#define SENDER_TIMEOUT 50
#define PACKET_TIMEOUT 1

class UDPSocket {
protected:
    uint32_t buffer_size;
    int net_socket;
    struct sockaddr_in address;
    // Method
    void setNetSocket();

public:
    virtual void run() = 0;
};

#endif //JARKOM_UDPSOCKET_H
