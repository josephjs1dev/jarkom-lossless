#include "Sender.hpp"

using namespace std;

string ip, file;
uint32_t wsize, bsize;
in_port_t port;

int get_parameter(int argv, char* argc[]) {
    bool done = false;
    for (int i = 1; i <= argv && !done; i++) {
        switch (i) {
            case 1:
                file = argc[i];
                break;
            case 2:
                wsize = (uint32_t) stoi((string) argc[i]);
                break;
            case 3:
                bsize = (uint32_t) stoi((string) argc[i]);
                break;
            case 4:
                ip = argc[i];
                break;
            case 5:
                port = (in_port_t) stoi((string) argc[i]);
                break;
            default:
                done = true;
        }
    }
    return (argv == 6) - 1;
}

void print_info() {
    printf("===================================\n");
    printf("Filename: %s\n", file.c_str());
    printf("Window Size: %d\n", wsize);
    printf("Buffer Size: %d\n", bsize);
    printf("IP: %s\n", ip.c_str());
    printf("Port: %d\n", port);
}

int main(int argc, char* argv[]) {
    if (get_parameter(argc, argv) == -1) {
        perror("Input must be: filename windowsize buffersize port\n");
        exit(1);
    }
    print_info();
    Sender s(file, wsize, bsize, ip, port);
    s.run();
    return 0;
}