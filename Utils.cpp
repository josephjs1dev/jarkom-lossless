//
// Created by joseph on 10/29/18.
//

#include <cstdio>
#include <cstdlib>
#include "Utils.hpp"

void handleError(char const *msg) {
    perror(msg);
    exit(1);
}


char* readFileContent(std::ifstream& is, size_t buf_size) {
    char* buffer = new char[buf_size];
    is.read(buffer, buf_size);
    return buffer;
}