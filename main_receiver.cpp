//
// Created by joseph on 10/27/18.
//

#include "Receiver.hpp"
#include <string>
#include <iostream>

std::string file;
uint32_t wsize, bsize;
in_port_t port;

using namespace std;

int get_parameter(int argv, char* argc[]) {
    bool done = false;
    for (int i = 1; i <= argv && !done; i++) {
        switch (i) {
            case 1:
                file = argc[i];
                break;
            case 2:
                wsize = (uint32_t) stoi((string) argc[i]);
                break;
            case 3:
                bsize = (uint32_t) stoi((string) argc[i]);
                break;
            case 4:
                port = (in_port_t) stoi((string) argc[i]);
                break;
            default:
                done = true;
        }
    }
    return (argv == 5) - 1;
}

void print_info() {
    printf("===================================\n");
    printf("Filename: %s\n", file.c_str());
    printf("Window Size: %d\n", wsize);
    printf("Buffer Size: %d\n", bsize);
    printf("Port: %d\n", port);
}

int main(int argv, char** argc) {
    if (get_parameter(argv, argc) == -1) {
        perror("Input must be: filename windowsize buffersize port\n");
        exit(1);
    }
    Receiver r(file, wsize, bsize, port);
    print_info();
    r.run();
    return 0;
}
